## Intro 01 - Textpost #####

Use AngularJS to create a simple page that can make text posts.

It takes less than 10 minutes if you know AngularJS. If you are new to AngularJS, it may require more time.

* A way for the user to add input text
* An Add Post button
* When the Add Post button is clicked, the input text is used to create a new post and the new post is displayed.

- - -
### Environment Setup via Bash #####
    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john

    #Set up git repo
    git clone git@gitlab.com:cl-/intro-project--angularjs--tg.git;
    cd intro-project--angularjs--tg/intro01-textpost;
    git branch;

    #Set up your branch
    git branch $yourName;
    git checkout $yourName;
    git push -u origin $yourName; #Sets the upstream branch

    #Launch a simple server (optional)
    python -m SimpleHTTPServer;

    #Open page in web browser
    open http://localhost:8000/textpost.html; #after you have already launched the simple server

- - -
### Environment Update via Bash #####
    #Look for your branch name
    git branch;

    #Set your lowercase name
    yourName=INSERT_YOUR_LOWERCASE_NAME_HERE;  #It should look like: yourName=john
    
    #Get updates
    git checkout $yourName;
    git fetch;
    git rebase master;

- - -
### Resources #####
If you are new to AngularJS, this is sufficient in providing the necessary basic details to get you started.

* http://www.w3schools.com/angular/
  * http://www.w3schools.com/angular/angular_intro.asp
  * http://www.w3schools.com/angular/angular_expressions.asp
  * http://www.w3schools.com/angular/angular_directives.asp
  * http://www.w3schools.com/angular/angular_controllers.asp
  * http://www.w3schools.com/angular/angular_filters.asp

* https://docs.angularjs.org/guide/introduction
  * https://docs.angularjs.org/guide/concepts
  * https://docs.angularjs.org/guide/expression
  * https://docs.angularjs.org/guide/controller
  * https://docs.angularjs.org/guide/filter
  * https://docs.angularjs.org/guide/databinding
  * https://docs.angularjs.org/guide/scope

<!--
* https://docs.angularjs.org/guide/introduction
  * https://docs.angularjs.org/guide/concepts
  * https://docs.angularjs.org/guide/expression
  * https://docs.angularjs.org/guide/controller
  * https://docs.angularjs.org/guide/directive##
  * https://docs.angularjs.org/guide/filter
  * https://docs.angularjs.org/guide/databinding
  * https://docs.angularjs.org/guide/scope
  * https://docs.angularjs.org/guide/animations##
-->

* https://docs.angularjs.org/api
  * https://docs.angularjs.org/api/ng/directive/ngController
  * https://docs.angularjs.org/api/ng/directive/ngInit
  * https://docs.angularjs.org/api/ng/directive/ngModel
  * https://docs.angularjs.org/api/ng/directive/ngRepeat

<!--
  &nbsp;
  * https://docs.angularjs.org/api/ng/directive/ngSrc
  * https://docs.angularjs.org/api/ng/directive/ngShow
  * https://docs.angularjs.org/api/ng/directive/ngHide
  * https://docs.angularjs.org/api/ng/directive/ngClick
  &nbsp;
  * https://docs.angularjs.org/api/ng/filter
    * https://docs.angularjs.org/api/ng/filter/currency
    * https://docs.angularjs.org/api/ng/filter/date
    * https://docs.angularjs.org/api/ng/filter/orderBy
    * https://docs.angularjs.org/api/ng/filter/limitTo
  &nbsp;
  * https://docs.angularjs.org/api/ng/directive/ngClass
-->

This official tutorial from AngularJS.org is too complicated for you and will confuse you if you are new to AngularJS, so do not look it for now: https://docs.angularjs.org/tutorial/index




